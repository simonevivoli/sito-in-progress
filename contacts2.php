<html>
<head>
    <title>Il mio script contacts2</title>
</head>
<body>
<h1>Ciao!</h1>
<p> Con questo secondo script non mando la mail, ma ti mostro le informazioni che ho ricevuto via POST HTTP</p>
<p>Come vedi nello script engine PHP posso recuperarle accedendo alla variabile <tt>$_POST</tt>.
Questa in PHP si chiama <a href="https://www.php.net/manual/en/language.variables.superglobals.php" target="_blank">Superglobal</a></p>

<p>Ovviamente tu non stai vedendo il codice php se accedi questa pagina tramite browser,
ma lo vedi se guardi il contenuto del file nel repository GitLab</p>

<p>Le righe comprese tra <pre>&lt;?php</pre> e <pre>?&gt;</pre> sono processate dallo script engine che ne sostituisce l'output nella pagina HTML</p>


<?php
echo "ciao";
if(isset($_POST['nome']) && isset($_POST['subject'])&& isset($_POST['email']))
{
	$fromEmail = $_POST['email'];
	$to = 'admin@mail.it';
	$subject = 'Messaggio '.$_POST['nome'];
	$headers = 'From: ' . $fromEmail . "\r\n" . 'Reply-To: ' . $fromEmail . "\r\n" . 'X-Mailer: PHP/' . phpversion();

  echo "--- Invio mail ---<br>";
  echo "Mittente: ".$_POST['nome']." (".$_POST['email'].")<br>";
  echo "Oggetto: ".$_POST['subject']."<br><br>";
  echo "Messaggio:<br>".$_POST['body'];

	# mail($to, $subject, $message, $headers);
}
else {
    echo "<div>Non hai passato alcun parametro con il metodo POST quindi non ho nulla da visualizzare</div>";
}

?>

</body>
</html>
